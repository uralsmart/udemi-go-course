package main

import (
	"bufio"
	"fmt"
	"math/rand"
	"os"
	"time"
)

func main() {
	rand.Seed(time.Now().UnixNano())
	var firstNumber = rand.Intn(8) + 2
	var secondNumber = rand.Intn(8) + 2
	var subtraction = rand.Intn(8) + 2
	var answer = firstNumber*secondNumber - subtraction
	playTheGame(firstNumber, secondNumber, subtraction, answer)
}
func playTheGame(firstNumber, secondNumber, subtraction, answer int) {
	const prompt = "and press enter when ready"
	fmt.Println("Guess the Number Game")
	fmt.Println("_________________________")
	fmt.Println("")
	fmt.Println("Enter a number between 1 and 10", prompt)
	var reader = bufio.NewReader(os.Stdin)
	reader.ReadString('\n')
	fmt.Println("Multiply your number by", firstNumber, prompt)
	reader.ReadString('\n')
	fmt.Println("Now multiply the result by", secondNumber, prompt)
	reader.ReadString('\n')
	fmt.Println("Divide the result by the your number", prompt)
	reader.ReadString('\n')
	fmt.Println("Now subtract", subtraction, prompt)
	reader.ReadString('\n')
	fmt.Println("The answer is", answer)
}
